<?php 
    include_once 'model/area.php';
    class RequisitoController extends Controller
    {
        function __construct()
        {
            
            parent :: __construct();
        }

        function mostrarVista()
        {
           
        }

        function radicarRequisito(){
            //
            /*Acá haré el código que recibe el $_POST y lo inserta en la BD
                en la tabla requisito y detallereq.
                Radicar un requisito sólo actualiza las columnas fkemple, fkreq y observacion
                de la tabla detallereq, y todas las columnas de la tabla requisito.
            */
            $area = new Area();
            $this->vista->areas = $area->getAllAreas();
            $req = $_POST['txtReq'];
            $area = $_POST['txtArea'];

            if($req && $area){
               $lastid= $this->model->createRequisito($area);

                if($lastid != 0){
                    $fecha = date('Y-m-d H:i:s');
                    $this->model->createDetalleReq([
                        'fecha' => $fecha,
                        'observ' => $req,
                        'fkemple' => 1152211862,
                        'fkreq' => $lastid
                    ]);
                    header("location: ../main?status=1");
                }
                else{
                    header("location: ../main?status=0");
                }
            }
           
            $this->vista->render("main/main");
           
            
        }
    }
?>