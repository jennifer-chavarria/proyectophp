<?php
    include_once 'model/area.php';
    include_once 'model/areaobject.php';
    include_once 'model/empleadoobject.php';
    class EmpleadoController extends Controller
    {
        function __construct()
        {
            parent ::__construct();
            $this->vista->empleados = [];
        }

        function mostrarVista()
        {
            $this->vista->mensaje ="";
           // $this->vista->render('empleado/ingresarEmpleado');
            
        }

        function renderAreas(){
            $area = new Area();
            $this->vista->areas = $area->getAllAreas();
            $this->vista->render('empleado/ingresarEmpleado');
        }
        
        function ingresarEmpleado(){
            $this->vista->mensaje ="";
            $this->renderAreas();
        }

        function guardarEmpleado(){
            $id = $_POST['txtId'];
            $nombre = $_POST['txtNom'];
            $tel = $_POST['txtTel'];
            $cargo = $_POST['txtCargo'];
            $email = $_POST['txtEmail'];
            $area = $_POST['txtArea'];
            if(isset($_POST['txtJefe'])){
                $jefe = $_POST['txtJefe'];

                if($this->model->update([
                    'id' => $id,
                    'nombre' => $nombre,
                    'tel' => $tel,
                    'cargo' => $cargo,
                    'email' => $email,
                    'area' => $area,
                    'jefe' => $jefe,
                ])){
                    $this->vista->mensaje ="Datos actualizados correctamente.";
                    $this->renderAreas();
                }
                else{
                    $this->vista->mensaje ="Error al actualizar datos.";
                    $this->renderAreas();
                }
            }
            else{
                if($this->model->create([
                    'IDEMPLEADO' => $id,
                    'NOMBRE' => $nombre,
                    'TELEFONO'=> $tel,
                    'CARGO' => $cargo,
                    'EMAIL' => $email,
                    'FKAREA' => $area,
                   'FKEMPLE' => null
                ])){
                    $this->vista->mensaje ="Datos guardados correctamente.";
                    $this->renderAreas();
                }
                else{
                    $this->vista->mensaje ="Error al guardar datos.";
                    $this->renderAreas();
                }
            }
        }

        function mostrarEmpleados(){
            $empleados = $this->model->getAll();
            $this->vista->empleados = $empleados;
            $this->vista->render('empleado/index');
        }

        function borrarEmpleado($id){
            $empleado = $this->model->getByID($id);
            $this->model->delete($id);
            $this->mostrarEmpleados();
        }

        function editarEmpleado($id){
            $area = new Area();
            $empleado = new EmpleadoObject();
            $empleado = $this->model->getByID($id);
            $this->vista->empleado = $empleado;
            $this->vista->areas = $area->getAllAreas();
            $this->vista->posiblesjefes = $this->model->getAll();
            $this->vista->mensaje ="";
            $this->vista->render('empleado/editar');
            //$empleado->nombre = $_POST['txtNom'];
            //$empleado->telefono = $_POST['txtTel'];
            //$empleado->cargo = $_POST['txtCargo'];
            //$empleado->email = $_POST['txtEmail'];
            //$empleado->area = $_POST['txtArea'];
            //$empleado->jefe = $_POST['txtJefe'];
            //$this->model->update($empleado);
            
        }
    }
?>