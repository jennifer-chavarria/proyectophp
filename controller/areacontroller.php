<?php
    //include_once 'model/area.php';
   // include_once 'model/areaobject.php';
    include_once 'model/empleado.php';
    include_once 'model/empleadoobject.php';
class AreaController extends Controller {


    function __construct()
    {
        parent ::__construct();
        $this->vista->areas = [];
        $this->vista->emple=[];
        $this->vista->mensaje =[];
    }


    function guardarArea(){

        $id = $_POST['txtIdArea'];
        $nombre = $_POST['txtNombre'];
        $empleado = $_POST['txtEmpleado'];

        
            if($this->model->create([
                'IDAREA' => $id,
                'NOMBRE' => $nombre,
                'FKEMPLE'=> $empleado,
            ])){
                $this->vista->mensaje ="Datos guardados correctamente.";
                $this->renderEmpleado();
            }
            else{
                $this->vista->mensaje ="Error al guardar datos.";
                $this->renderEmpleado();
            }
        
        

        
    }
     function editarAreaDato(){

        $id = $_POST['txtIdArea'];
        $nombre = $_POST['txtNombre'];
        $empleado = $_POST['txtEmpleado'];
        
            if($this->model->update([
                'IDAREA' => $id,
                'NOMBRE' => $nombre,
                'FKEMPLE'=> $empleado,
            ])){
                $this->vista->mensaje ="Datos guardados correctamente.";
                $this->editarArea($id);
            }
            else{
                $this->vista->mensaje ="Error al guardar datos.";
                $this->editarArea($id);
            }



     }

    function mostrarVista(){  
      $area1 = new Area();
        $this->vista->areas = $area1->getAllAreas();     
        $this->vista->render('area/index');
    }


    function renderEmpleado(){
        $empleadoDao = new Empleado();
        $this->vista->emple = $empleadoDao->getAll();
        $this->vista->render('area/ingresarArea');
    }
    
    function ingresarArea(){
        $this->vista->mensaje ="";
        $this->renderEmpleado();
    }

    function borrarArea($id){
        $area2 = $this->model->getAreaByID($id[0]);
        $this->model->delete($id[0]);
        $this->mostrarVista();
    }

    function editarArea($id){
        $empleado = new Empleado();
        $area3 = new AreaObject();
        $area3 = $this->model->getAreaByID($id[0]);
        $this->vista->area = $area3;
        $this->vista->empleados = $empleado->getAll();
        //$this->vista->posiblesempleados = $this->model->getAllArea()
        
        $this->vista->render('area/editar');
        //$empleado->nombre = $_POST['txtNom'];
        //$empleado->telefono = $_POST['txtTel'];
        //$empleado->cargo = $_POST['txtCargo'];
        //$empleado->email = $_POST['txtEmail'];
        //$empleado->area = $_POST['txtArea'];
        //$empleado->jefe = $_POST['txtJefe'];
        //$this->model->update($empleado);
        
    }

/*
    // aun no he mirado si lo uso
    function mostrarArea(){
        $empleados = $this->model->getAll();
        $this->vista->empleados = $empleados;
        $this->vista->render('empleado/index');
    }
*/
}






?>