<?php 
    include_once 'model/area.php';
    class MainController extends Controller
    {
        function __construct()
        {

            parent :: __construct();
            if($_GET  && isset($_GET['status'])){
                if($_GET['status']=='1'){
                    $this->vista->mensaje= "Requisito Radicado Correctamente";
                }
                else{
                    $this->vista->mensaje= "Error al guardar los datos.";
                }
            }
            else{
                $this->vista->mensaje= "";
            }
            
        }

        function mostrarVista()
        {
            //require 'view/vistaprueba.php';
            $area = new Area();
            $this->vista->areas = $area->getAllAreas();
            $this->vista->render('main/main');
        }
    }
?>