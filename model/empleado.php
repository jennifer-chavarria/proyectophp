<?php 
    include_once 'model/empleadoobject.php';
    //include_once 'model/area.php';
    include_once 'model/areaobject.php';

    class Empleado extends Model{

        function __construct()
        {
            parent ::__construct();
        }

        public function create($datos)
        {
            $query = $this->db->connect()->prepare('INSERT INTO empleado VALUES (:a, :b, :c, :d, :e, :f, :g)');

            try{
                $query->execute([
                    'a' => $datos['IDEMPLEADO'],
                    'b' => $datos['NOMBRE'],
                    'c' => $datos['TELEFONO'],
                    'd' => $datos['CARGO'],
                    'e' => $datos['EMAIL'],
                    'f' => $datos['FKAREA'],
                    'g' => null
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }

        public function getAll(){
            $items = [];
            $areaDao = new Area();
            try{
                $query = $this->db->connect()->prepare('SELECT * FROM empleado');
                $query->execute();
                while($row = $query->fetch()){
                    $item = new EmpleadoObject();
                   //area = new AreaObject();
                    $item->idempleado = $row['IDEMPLEADO'];
                    $item->nombre     = $row['NOMBRE'];
                    $item->telefono   = $row['TELEFONO'];
                    $item->cargo      = $row['CARGO'];
                    $item->email      = $row['EMAIL'];
                    $item->area  = $areaDao->getAreaByID($row['FKAREA'])->nombre;
                    if($row['FKEMPLE'] != null){
                        $idjefe = $row['FKEMPLE'];
                        $item->jefe  = $this->getByID($idjefe)->nombre;
                    }
                    
                    array_push($items, $item);
                }
                return $items;
            }catch(PDOException $e){
                return [];
            }
        }

        /**
         * $id es un array que viene de la URL.
         */
        public function getByID($id){
            $normalizedId = "";
            if(is_array($id)){
                $normalizedId = $id[0];
            }else{
                $normalizedId = $id;
            }
            $item = new EmpleadoObject();

            try{
                $query =$this->db->connect()->prepare('SELECT * FROM empleado WHERE IDEMPLEADO ='.$normalizedId);
                $query->execute();
                while($row = $query->fetch()){
                    $item->idempleado = $row['IDEMPLEADO'];
                    $item->nombre     = $row['NOMBRE'];
                    $item->telefono   = $row['TELEFONO'];
                    $item->cargo      = $row['CARGO'];
                    $item->email      = $row['EMAIL'];
                    $item->area       = $row['FKAREA'];
                    $item->jefe       = $row['FKEMPLE'];
                }
            return $item;
            }catch(PDOException $e){
                
            }
        }

        public function delete($id){
            try{
                $query =$this->db->connect()->prepare('DELETE FROM empleado WHERE IDEMPLEADO ='.$id[0]);
                $query->execute();
            }catch(PDOException $e){
                
            }
        }

        public function update($empleado){

            try{
                $query = $this->db->connect()->prepare('UPDATE empleado SET NOMBRE = :nom, TELEFONO = :tel, CARGO = :cargo, EMAIL = :email, FKAREA = :area, FKEMPLE = :jefe WHERE IDEMPLEADO = :id');
                $query->execute([
                    'nom' => $empleado['nombre'],
                    'tel' => $empleado['tel'],
                    'cargo' => $empleado['cargo'],
                    'email' => $empleado['email'],
                    'area' => $empleado['area'],
                    'jefe' => $empleado['jefe'],
                    'id' => $empleado['id']
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }
?>