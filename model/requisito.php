<?php 

    class Requisito extends Model{

        function __construct()
        {
            parent ::__construct();
        }


        public function createRequisito($datos)
        {
            $connection = $this->db->connect();
            $query = $connection->prepare('INSERT INTO requisito (FKAREA) VALUES (:a)');

            try{
                $query->execute([
                    'a' => $datos,
                ]);
                $lastid = $connection->lastInsertId();
                return $lastid;
            }catch(PDOException $e){
                return null;
            }
        }

        public function createDetalleReq($datos){
            $query = $this->db->connect()->prepare('INSERT INTO detallereq (FECHA, OBSERVACION, FKEMPLE, FKREQ, FKESTADO, FKEMPLEASIG) VALUES (:a, :b, :c, :d, :e, :f)');

            try{
                $query->execute([
                    'a' => $datos['fecha'],
                    'b' => $datos['observ'],
                    'c' => $datos['fkemple'],
                    'd' => $datos['fkreq'],
                    'e' => 1,
                    'f' => null
                    ]);
            }
            catch(PDOException $e){

            }
        }

        public function getAreaByID($id){
            $area = new AreaObject();
            

            try{
                $query = $this->db->connect()->prepare('SELECT * FROM area where IDAREA= '.$id);
                $query->execute();
                while($row = $query->fetch()){
                    $area->id = $row['IDAREA'];
                    $area->nombre = $row['NOMBRE'];
                    $area->encargado = $row['FKEMPLE'];
                }
                return $area;
            }catch(PDOException $e){
                echo 'id es '.$id;
                echo '';
                echo 'respuesta'.$e;
            }
        }


        public function getAllAreas(){
            $areas = [];
            
            try{
                $query = $this->db->connect()->prepare('SELECT * FROM area');
                $query->execute();
                while($row = $query->fetch()){
                    $area = new AreaObject();
                    $area->id = $row['IDAREA'];
                    $area->nombre = $row['NOMBRE'];
                    $area->encargado = $row['FKEMPLE'];
                    array_push($areas, $area);
                }
                return $areas;
            }catch(PDOException $e){

            }
        }
        public function delete($id){
            try{
                $query =$this->db->connect()->prepare('DELETE FROM area WHERE IDAREA ='.$id[0]);
                $query->execute();
            }catch(PDOException $e){
                
            }
        }

        public function update($area){

            try{
                $query = $this->db->connect()->prepare('UPDATE area SET NOMBRE = :nombre, FKEMPLE = :empleado WHERE IDAREA = :id ');
                $query->execute([
                    'nombre' => $area['NOMBRE'],
                    'empleado' => $area['FKEMPLE'],
                    'id' => $area['IDAREA']
                ]);

                return true;
            }catch(PDOException $e){            
            
               
                return false;
            }
        }
    }
?>