<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ingresar Empleado</title>
    
</head>
<body>
    <?php require 'view/static/header.php'?>
    <h5 class="center">Registro de Empleados</h5>
    <div class="container">

            <form class="col s12" method="post" action="<?php echo constant('URL');?>area/editarAreaDato">

                <div class="row">
                    <div class="input-field col s6">
                        <i class="material-icons prefix">#</i>
                        <input id="first_name" type="number" class="validate" name="txtIdArea" value ="<?php echo $this->area->id;?>">
                        <label for="first_name">ID Area</label>
                    </div>
                    <div class="input-field col s6">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="icon_prefix" type="text" class="validate" name="txtNombre" value ="<?php echo $this->area->nombre;?>">
                        <label for="icon_prefix">Nombre area</label>
                    </div>
                </div>

                    <div class="input-field col s6">
                        <select name="txtEmpleado">
                            <?php 
                                include_once 'model/empleadoobject.php';
                                foreach($this->empleados as $registro){
                                    $empleadoobject = new EmpleadoObject();
                                    $empleadoobject = $registro;

                                    if($empleadoobject->id == $this->area->id){
                            ?>
                                    <option value="<?php echo $empleadosobject->idempleado;?>" selected><?php echo $areasobject->nombre;?></option>
                                <?php }
                                    else {
                                ?>       
                                    <option value="<?php echo $empleadoobject->idempleado;?>"><?php echo $empleadoobject->nombre;?></option> 
                                <?php   
                                    }
                                    } ?>
                        </select>
                        <label>Área</label>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <button type="submit" class="btn waves-effect waves-light" name="btnGuardar">Guardar
                            <i class="material-icons right">save</i>
                        </button>
                    </div>
                    <div class="input-field col s6">
                            <a class ="btn waves-effect waves-light" href="<?php echo constant('URL');?>area/mostrarVista">Volver a Tabla de Empleados</a>
                    </div>
                </div>
            </form>
    </div>
    


    <?php require 'view/static/footer.php' ?>
    <script src="<?php echo constant('URL');?>resources/js/select.js"></script>
</body>
</html>