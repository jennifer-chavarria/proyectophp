<html>

<head>
  <link rel="stylesheet" type="text/css" href="<?php echo constant('URL');?>resources/css/login.css">
</head>

<body>
    <?php require 'view/static/header.php'?>
  <div class="section"></div>
  <main>
    <center>
    <div class="section"></div>
    <div class="section"></div>

      <h5 class="deep-orange-text accent-3">Ingreso de Usuarios</h5>
      
      <div class="container">
        <div class="z-depth-1 white lighten-4 row hoverable card custom-box">
          <form class="col s12" method="post" action="<?php echo constant('URL');?>login/login">
              
            <div class='row'>
              <div class='input-field col s12'>
                <i class="material-icons prefix">account_box</i>
                <input class='validate' type='text' name='txtUser' id='email' />
                <label for='email'>Ingrese su usuario</label>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <i class="material-icons prefix">vpn_key</i>
                <input class='validate' type='password' name='txtPass' id='password' />
                <label for='password'>Ingrese su contraseña</label>
              </div>
            </div>
            <center>
              <div class='row'>
                <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect deep-orange accent-3'>Login</button>
              </div>
            </center>
          </form>
        </div>
        <h5><?php if($this->mensaje != null)
                    {echo $this->mensaje;}  ?>
        </h5>
      </div>
    </center>

    <div class="section"></div>
    <div class="section"></div>
  </main>

  <?php require 'view/static/footer.php'?>
</body>

</html>