<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo constant('URL');?>resources/css/textarea.css">
    <title>Document</title>
</head>
<body>
    <?php require 'view/static/header.php'?>

        <div class="section">
            <h4 class="center">Mesa de Ayuda</h4>
            <div class="container">
                <div class="section"></div>
                <div class="divider"></div>
                
                <div class="nav-container">
                    <ul id="slide-out" class="sidenav">
                        <li><div class="section"></div></li>
                        <li><div class="section"></div></li>
                        <li><a class="subheader">Seleccione una de las opciones</a></li>
                        <li><a id="adminUsuarios" href="#" style="color: #000000">
                                <i class="material-icons">laptop</i>
                                <p class="promo-caption">Administración de Usuarios</p>
                            </a>
                        </li>
                        <li><a id="adminUsuarios" href="<?php echo constant('URL');?>maestros" style="color: #000000">
                                <i class="material-icons">remove_red_eye</i>
                                <p class="promo-caption">Gestión de Maestros</p>
                            </a>
                        </li>
                        <li><a id="adminUsuarios" href="#" style="color: #000000">
                                <i class="material-icons">search</i>
                                <p class="promo-caption">Consultas e Informes</p>
                            </a>
                        </li>
                    </ul>
                    
                </div>  

                <div class="section"></div>
                <div class="section"></div>

                <div class="">
                    <a href="#" data-target="slide-out" class="sidenav-trigger place-right custom-color">Abrir Menú</a>
                </div>

                <form action="requisito/radicarRequisito" method="post" class="col s12">
                    <div class="row">
                        <div class="input-field col s6">
                            <textarea style="width: 738px; height: 253px;" name="txtReq" placeholder="Radicar Requisito: Mensaje"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s3">
                            <button type="submit" class="btn waves-effect deep-orange accent-3" name="btnGuardar">Radicar
                            </button>
                        </div>
                        <div class="input-field col s5">
                        <select name="txtArea">
                            <option value="" disabled selected>AreaRequisito</option>
                            <?php 
                                include_once 'model/areaobject.php';
                                foreach($this->areas as $registro){
                                    $areasobject = new AreaObject();
                                    $areasobject = $registro;
                            ?>
                                <option value="<?php echo $areasobject->id;?>"><?php echo $areasobject->nombre;?></option>
                                <?php } ?>
                        </select>
                        <label>Área</label>
                        </div>
                    </div>
                </form>
                <h5><?php if($this->mensaje != null)
                    {echo $this->mensaje;}  ?>
            </h5>
            </div> 
        </div>

       
    <?php require 'view/static/footer.php'?>
    <script src="<?php echo constant('URL');?>resources/js/util.js"></script>
    <script src="<?php echo constant('URL');?>resources/js/select.js"></script>
    
</body>
</html>