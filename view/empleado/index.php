<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php require 'view/static/header.php'?>
    <div class="container">
        <div id="empleados-container">
            <table width=100%>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Teléfono</th>
                        <th>Cargo</th>
                        <th>Email</th>
                        <th>Área</th>
                        <th>Jefe</th>
                    </tr>
                </thead>
                <tbody id="tbody-empleados">
                    <?php 
                        include_once 'model/empleadoobject.php';
                        foreach($this->empleados as $registro){
                            $empleado = new EmpleadoObject();
                            $empleado = $registro;
                    ?> 
                    <tr id="trow-empleados-<?php echo $empleado->idempleado;?>">
                        <td><?php echo $empleado->idempleado;?></td>
                        <td><?php echo $empleado->nombre;?></td>
                        <td><?php echo $empleado->telefono;?></td>
                        <td><?php echo $empleado->cargo;?></td>
                        <td><?php echo $empleado->email;?></td>
                        <td><?php echo $empleado->area;?></td>
                        <td><?php echo $empleado->jefe;?></td>
                        <td><a href="<?php echo constant('URL');?>empleado/editarEmpleado/<?php echo $empleado->idempleado;?>"><i class="material-icons left">edit</i></a></td>
                        <td><a href="<?php echo constant('URL');?>empleado/borrarEmpleado/<?php echo $empleado->idempleado;?>"><i class="material-icons left">delete</i></a></td>      
                    </tr>       
                    <?php } ?>
                </tbody>
            </table>
        
        </div>
        
    </div>
    
    <a href="<?php echo constant('URL');?>empleado/ingresarEmpleado"class="btn deep-orange accent-3">Ingresar nuevo empleado</a>
    <?php require 'view/static/footer.php'?>
    <script src="<?php echo constant('URL');?>resources/js/util.js"></script>
</body>
</html>