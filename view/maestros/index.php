<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php require 'view/static/header.php'?>
    <div class="section">  
            <h3 class="center">Gestión de Maestros</h3>
            <h5 class="center">Elija una de las opciones disponibles: </h5>
            <div class="container">
                <div class="section"></div>
                <div class="divider"></div>
                <div class="section"></div>
                <div class="row">
                        <div class="col s4">
                            <a id="adminUsuarios" href="<?php echo constant('URL');?>area/mostrarVista" style="color: #000000">
                                <div class="center promo">
                                    <i class="material-icons">laptop</i>
                                    <p class="promo-caption">Áreas</p>
                                    <p class="light center">Aquí dice algo acerca de las áreas</p>
                                </div>
                            </a>
                        </div>
                        <div class="col s4">
                            <a id="adminUsuarios" href="<?php echo constant('URL');?>empleado/mostrarEmpleados" style="color: #000000">
                                <div class="center promo">
                                    <i class="material-icons">remove_red_eye</i>
                                    <p class="promo-caption">Empleados</p>
                                    <p class="light center">Aquí dice algo acerca de la gestión de maestros</p>
                                </div>
                            </a>
                        </div>
                        <div class="col s4">
                            <a id="adminUsuarios" href="#" style="color: #000000">
                                <div class="center promo">
                                    <i class="material-icons">search</i>
                                    <p class="promo-caption">Estado de Requisitos</p>
                                    <p class="light center">Aquí dice algo acerca de los estados de los requisitos</p>
                                </div>
                            </a>
                        </div>
                        
                </div>
            </div> 
        </div>
    
    <?php require 'view/static/footer.php'?>
</body>
</html>