<?php 

    class MainApp
    {
        function __construct()
        {
            $url = isset($_GET['url']) ? $_GET['url'] : null;

            //La aplicación va a tener un mapeo de URL de la siguiente manera:
            //{path_base}/controlador/metodo/parametro
            //Por eso dividimos esto en un array.
            $url = explode("/", $url);

            //var_dump($url);

            //Cuando no se ingresa controlador
            if(empty($url[0])){
                require_once 'controller/logincontroller.php';
                //Cargamos el controlador principal, su modelo y su vista
                $controller = new LoginController();
                //$controller->loadModel('main');
                $controller->mostrarVista();
                return false;
            }

            $archivoController = 'controller/'.$url[0] . 'controller.php';

            if(file_exists($archivoController))
            {
                require_once $archivoController;

                $className = $url[0].'Controller';
                
                $controller = new $className;
                $controller->cargarModelo($url[0]);

                $urlTamano = sizeof($url);

                if($urlTamano>1)
                {
                    if($urlTamano>2)
                    {
                        $param = [];

                        for($i=2; $i<$urlTamano; $i++)
                        {
                            array_push($param, $url[$i]);
                        }
                        $controller->{$url[1]}($param);
                    }
                    else
                    {
                        $controller->{$url[1]}();
                    }
                }
                else
                {
                    $controller->mostrarVista();
                }
            }
            else
            {
                //$controller = new ErrorHandler();
            }
            
            
        }
    }
?>